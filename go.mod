module gitlab.com/kendellfab/do-go-app

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/gorilla/sessions v1.1.3
	gitlab.com/kendellfab/fazer v0.12.3
)
