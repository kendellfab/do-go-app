package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/gorilla/sessions"
	"gitlab.com/kendellfab/fazer"
	"log"
	"net/http"
	"os"
)

func main() {

	store := sessions.NewCookieStore([]byte("2390234ADSFAewowe"))
	fzr := fazer.NewFazer(store, "static/tpls", nil, false)
	fzr.RegisterTemplate("head", "front/head.gohtml")
	fzr.RegisterTemplate("foot", "front/foot.gohtml")
	fzr.RegisterTemplate("front.index", "front/index.gohtml")
	fzr.RegisterTemplate("front.about", "front/about.gohtml")

	mux := chi.NewMux()
	mux.Get("/", func(w http.ResponseWriter, r *http.Request) {
		fzr.RenderTemplate(w, r, nil, "front.index")
	})

	mux.Get("/about", func(w http.ResponseWriter, r *http.Request) {
		fzr.RenderTemplate(w, r, nil, "front.about")
	})

	log.Println("Started do go app.")
	http.ListenAndServe(fmt.Sprintf(":%s", GetEnvOrDefault("PORT", "3000")), mux)
}

func GetEnvOrDefault(key, def string) string {
	val := os.Getenv(key)
	if val != "" {
		return val
	}

	return def
}